package com.amazon.test;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

	private WebDriver driver;
	private Popup popup;

	@Before
	public void setUp() throws Exception {
		// On récupére le système d'explotation
		String os = System.getProperty("os.name")
				.toLowerCase()
				.split(" ")[0];
		// générer le chemin du fichier du driver
		String pathMarionette = Paths.get(".").toAbsolutePath().normalize().toString()+"/lib/chromedriver-"+os;
		
		// enregistre le chemin dans une propriété qui est webdriver.chrome.driver
		// Firefox : webdriver.gecko.driver
		System.setProperty("webdriver.chrome.driver", pathMarionette);

		// options pour mettre le navigateur en pleine écran
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");

		// on créé l'objet ChromeDriver
		driver = new ChromeDriver(options);
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);	
		
		// création de l'objet Popup
		popup = new Popup(driver, "a-popover-1", "//*[@id=\"a-popover-1\"]/div/header/button");
	}	
	
  	
	@After
	public void tearDown() throws Exception {
		// on ferme le driver ouvert
		driver.close();
		driver.quit();
	}		  

}

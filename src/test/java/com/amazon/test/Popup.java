package com.amazon.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;
import org.openqa.selenium.support.ui.WebDriverWait;
 
public class Popup {
 
  private final WebDriverWait wait;
  private final By popupIdLocator;
  private final By closeElementLocator;
  public Popup(WebDriver driver, String popupIdLocator, String closeElementLocator) {
    this.wait = new WebDriverWait(driver, 2);
    this.popupIdLocator = By.id(popupIdLocator);
    this.closeElementLocator = By.xpath(closeElementLocator);
  }
 
  public void close() throws InterruptedException {
    if (isDisplayed()) {
        WebElement closeElement = wait.until(
               visibilityOfElementLocated(closeElementLocator));
        closeElement.click();
    }
  }





  private Boolean isDisplayed() throws InterruptedException {
     try {
        WebElement popup = wait.until(
                visibilityOfElementLocated(popupIdLocator));
        return popup.isDisplayed();
     }
     catch (Exception ex) {
       return false;
     }
 }
}